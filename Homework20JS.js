let listNames = [{
    name: 'Toyota',
    contentType: 'en',
    locales: 'Japan',
    description: 'Import cars from Japan'
    },
    {
    name: 'Nissan',
    contentType: 'US',
    locales: 'Japan',
    description: 'Import cars from Japan'
    },
    {
    name: 'BMW',
    contentType: 'en_US',
    locales: 'Germany',
    description: 'Import cars from Germany'
    },
    {
    name: 'Honda',
    contentType: 'en',
    locales: 'Japan',
    description: 'Import cars from Japan'
    },
    {
    name: 'Mercedes-Benz',
    contentType: 'US',
    locales: 'Germany',
    description: 'Import cars from Germany'
    },
    {
    name: 'Volkswagen',
    contentType: 'en_US',
    locales: 'Germany',
    description: 'Import cars from Germany'
    }
];
// console.log(listNames);

function filterCollection(arr, prop, value){
    let vehicles = [];
        copy = [...arr]
    for (const item of copy) {
        if (String(item[prop]).includes(value)== true) vehicles.push(item)
}
    return vehicles

}

function render(arr) {
    const list = document.querySelector('.vehicles-list');
    list.innerHTML = ''

    const nameVal = document.getElementById('inp-name').value,
          contentVal = document.getElementById('inp-content').value,
          localesVal = document.getElementById('inp-locales').value,
          descriptionVal = document.getElementById('inp-description').value

    let newArr = [...arr]
    if(nameVal !=='') newArr = filterCollection(newArr, 'name', nameVal)
    if(contentVal !=='') newArr = filterCollection(newArr, 'contentType', contentVal)
    if(localesVal !=='') newArr = filterCollection(newArr, 'locales', localesVal)
    if(descriptionVal !=='') newArr = filterCollection(newArr, 'description', descriptionVal)

    for (const name of newArr) {
        const li = document.createElement('li')
        li.textContent = name.name + ", " + name.contentType + ", " + name.locales + ", " + name.description
        list.append(li)
    }
}

document.getElementById('filter-form').addEventListener('submit', function(event) {
    event.preventDefault()
    render(listNames)
})
render(listNames)